<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaikèn</title>
    </head>
    <body>
        <!--TODO cambiar estilo--> 
        <header><h1>Menú de Cuidador</h1></header>
        <form method="post" action="cuidador">
            <h2 align="center">Bienvenido cuidador ${usuarioElegido}!</h2>
            <div class="container" align="center">
            <p>
                Seleccione la opción deseada:
                <select name="opcion" size="1">
                    <option value="datos"> Consultar datos personales </option>
                    <option value="consultarEspecie"> Consultar especies </option>
                    <option value="salir"> Salir </option>
                </select>
                <input type="submit" value ="Enviar">
            </p>
            </div>
        </form>
    </body>
</html>
