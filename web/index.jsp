<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" href="css/estilo.css">
        <link rel="icon" type="image/x-icon" href="img/favicon.ico">
        <link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico">
        <title>Bioparque Temaiken</title>
    </head>
    <body>
        <header><h1>Bioparque Temaiken</h1></header>
        <div class="container" align="center">
            <form method="post" action="index">
                <h1 align = "center"> Login Temaiken </h1>
                <section class="interests">
                    <p class="center">
                        <label for="usuario">Usuario:</label>
                        <input type = "text" id="usuario" class="form-control" name = "usuario">
                    </p>
                    <p class="center">
                        <label for="password">Contraseña:</label>
                        <input type = "password" id="password" class="form-control" name = "password">
                    </p>
                    <p class="center">
                        <button type="submit" name="entrar" class="btn btn-primary">Iniciar sesión</button>
                    </p>
                </section>
            </form>
        </div>
    </body>
</html>

