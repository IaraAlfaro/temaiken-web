package temaiken.bean;

/**
 * 
 * @author bianca
 */
public class EspeciesBean {
   
    private String especie;
    private String nombreCientifico;
    private String descripcion;
    private String esCuidadoDesde;
    private String cuidador;

    public String getEspecie() {
        return especie;
    }

    public void setEspecie(String especie) {
        this.especie = especie;
    }

    public String getNombreCientifico() {
        return nombreCientifico;
    }

    public void setNombreCientifico(String nombreCientifico) {
        this.nombreCientifico = nombreCientifico;
    }

     public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }
    
    public String getEsCuidadoDesde() {
        return esCuidadoDesde;
    }

    public void setEsCuidadoDesde(String esCuidadoDesde) {
        this.esCuidadoDesde = esCuidadoDesde;
    }

    public String getCuidador() {
        return cuidador;
    }

    public void setCuidador(String cuidador) {
        this.cuidador = cuidador;
    }
    
    
}
