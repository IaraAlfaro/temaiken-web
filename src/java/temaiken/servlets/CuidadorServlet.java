/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import temaiken.bean.EspeciesBean;
import temaiken.dao.CuidadorDAO;
import temaiken.dao.EspeciesDAO;
import temaiken.modelo.enums.JSPs;
import temaiken.modelo.enums.Opciones;

/**
 *
 * @author bianca
 */
public class CuidadorServlet extends HttpServlet {

    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws IOException, ServletException {

        String opcion = request.getParameter("opcion");
        CuidadorDAO cDao = new CuidadorDAO();

        if (Opciones.CONSULTAR_DATOS.getOption().equals(opcion)) {
            cDao.consultarDatos(Controlador.getUser());
            request.setAttribute("nombre", cDao.getResultado().getNombre());
            request.setAttribute("direccion", cDao.getResultado().getDireccion());
            request.setAttribute("telefono", cDao.getResultado().getTelefono());
            request.setAttribute("fechaIngreso", cDao.getResultado().getFechaIngreso());
            //busqueda en la base de los datos del usuario  
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.DATOS.getJsp());
            vista.forward(request, response);
        }
        if (Opciones.CONSULTAR_ESPECIES.getOption().equals(opcion)) {
            List<EspeciesBean> list = new ArrayList<>();
            EspeciesDAO dao = new EspeciesDAO();

            list = dao.getEspeciesByUser(Controlador.getUser());
            request.setAttribute("user", Controlador.getUser().toUpperCase());

            request.setAttribute("listaEspecies", list);
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.CUIDADOR_ESPECIES.getJsp());
            vista.forward(request, response);
        }
        if (Opciones.SALIR.getOption().equals(opcion)) {
            RequestDispatcher vista = request.getRequestDispatcher(JSPs.INDEX.getJsp());
            vista.forward(request, response);
        }
    }
}
