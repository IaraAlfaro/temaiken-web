package temaiken.servlets;

import temaiken.vista.EntradaSalida;
import temaiken.modelo.Habitat;
import temaiken.modelo.Usuario;
import temaiken.modelo.Zona;
import temaiken.modelo.Guia;
import temaiken.modelo.Temaiken;
import temaiken.modelo.Especie;
import temaiken.modelo.Cuidador;
import temaiken.modelo.Administrador;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Control {

    public void ejecutar() {

        Temaiken temaiken = new Temaiken();

        boolean seguir = true;
        try {
            temaiken = temaiken.deSerializar("temaiken.txt");
            EntradaSalida.mostrarString("SISTEMA DE TEMAIKEN");
        } catch (Exception e) {
            temaiken = cargarDatos();
            try {
                temaiken.serializar("temaiken.txt");
                EntradaSalida.mostrarString("El arranque ha sido exitoso. Ahora se debe reiniciar el sistema...");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            seguir = false;
        }

        while (seguir) {
            String usuario = EntradaSalida.leerString("Ingrese el usuario:");
            String password = EntradaSalida.leerPassword("Ingrese la password:");
            
            //devuelve un objeto de tipo guia o administrador o cuidador
            Usuario u = temaiken.buscarUsuario(usuario + ":" + password);

            if (u == null) {
                EntradaSalida.mostrarString("ERROR: La combinacion usuario/password ingresada no es valida.");
            } else {
                // POLIMORFISMO, en tiempo de ejecucion dependiendo de que tipo 
                //de usuario sea u, va a llamar al metodo proceder que corresponda, de la instancia que sea.
                u.proceder(temaiken);//le paso temaiken para que tenga acceso a todos los objetos que necesita 
                //para sus casos de uso, tengo la lista de usuarios, lista de zonas, lista de especies.
                seguir = EntradaSalida.leerBoolean("Desea loguearse con otro usaurio?");
            }
            
            try {
                temaiken.serializar("temaiken.txt");
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
    
    private Temaiken cargarDatos(){
        Temaiken parque;
        
        ArrayList<Especie> especies = new ArrayList<>();
        
        Habitat habitat1 = new Habitat("Habitat 1", "Clima 1", "Vegetacion 1", "Continente 1");
        Habitat habitat2 = new Habitat("Habitat 2", "Clima 2", "Vegetacion 2", "Continente 2");
        Habitat habitat3 = new Habitat("Habitat 3", "Clima 3", "Vegetacion 3", "Continente 3");
        Habitat habitat4 = new Habitat("Habitat 4", "Clima 4", "Vegetacion 4", "Continente 4");
        
        Especie especie1 = new Especie("Nombre Español 1", "Nombre científico 1", "Descripcion 1");
        Especie especie2 = new Especie("Nombre Español 2", "Nombre científico 2", "Descripcion 2");
        Especie especie3 = new Especie("Nombre Español 3", "Nombre científico 3", "Descripcion 3");
        Especie especie4 = new Especie("Nombre Español 4", "Nombre científico 4", "Descripcion 4");
        
        especies.add(especie1);
        especies.add(especie2);
        especies.add(especie3);
        especies.add(especie4);
        
        especie1.getHabitats().add(habitat1);
        especie2.getHabitats().add(habitat1);
        especie2.getHabitats().add(habitat2);
        especie3.getHabitats().add(habitat3);
        especie4.getHabitats().add(habitat4);
        
        ArrayList<Usuario> usuarios = new ArrayList<>();
        
        usuarios.add(new Administrador("Mauro", "Victorica 903, Escobar", "3907-2354", LocalDate.of(2018, 6, 1), "admin", "1234"));
        usuarios.add(new Cuidador("Joana", "Triunvirato 3908, CABA", "4558-2342", LocalDate.of(2018,6,16), "chowfan", "1234"));
        usuarios.add(new Guia("Iara", "Laprida 235, Ramos Mejia", "4657-2930", LocalDate.of(2018,6,16), "iaru", "1234"));
        
        usuarios.add(new Cuidador("José", "Cisneros 23, Escobar", "4563-9273", LocalDate.of(2018,6,16), "jose", "1234"));
        usuarios.add(new Guia("María", "Estrada 203, Escobar", "2343-3341", LocalDate.of(2018,6,16), "maria", "1234"));
        
        ArrayList<Zona> zonas = new ArrayList<>();
        
        Zona zona1 = new Zona("Zona 1", 30);
        Zona zona2 = new Zona("Zona 2", 20);
        Zona zona3 = new Zona("Zona 3", 100);
        Zona zona4 = new Zona("Zona 4", 50);
        
        zonas.add(zona1);
        zonas.add(zona2);
        zonas.add(zona3);
        zonas.add(zona4);
        
        zona1.getEspecies().add(especie1);
        zona2.getEspecies().add(especie2);
        zona3.getEspecies().add(especie3);
        zona4.getEspecies().add(especie4);
        
        parque = new Temaiken(especies, usuarios, zonas);
        
        return parque;
    }
}