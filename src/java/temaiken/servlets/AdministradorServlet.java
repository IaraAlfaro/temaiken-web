package temaiken.servlets;

import javax.servlet.*;
import javax.servlet.http.*;
import java.io.*;
import temaiken.dao.CuidadorDAO;

/**
 *
 * @author bianca
 */

public class AdministradorServlet extends HttpServlet {


    @Override
    public void doPost(HttpServletRequest request,HttpServletResponse response)
            throws IOException, ServletException {

        String opcion = request.getParameter("opcion");
        CuidadorDAO m = new CuidadorDAO();
        m.consultarDatos(Controlador.getUser());

       if(m.getResultado().getRol().equals("administrador")){
            request.setAttribute("usuarios", m.getResultado());
            request.setAttribute("usuarioElegido", Controlador.getUser());
        
            RequestDispatcher vista = request.getRequestDispatcher("administrador.jsp");
            vista.forward(request, response);
        }
    }
}
