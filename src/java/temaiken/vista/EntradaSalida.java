package temaiken.vista;

import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.Timer;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;

public class EntradaSalida {

    public static char leerChar(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        return (st == null || st.length() == 0 ? '0' : st.charAt(0));
    }

    public static String leerString(String texto) {
        String st = JOptionPane.showInputDialog(texto);
        return (st == null ? "" : st);
    }
    
    public static int leerInt(String texto) {
        boolean correcto = false;
        int entero = 0;
        
        while(!correcto) {
            try {
                String st = JOptionPane.showInputDialog(texto);
                entero = Integer.parseInt(st);
                correcto = true;
            }
            catch(NumberFormatException ex){}
        }
        
        return entero;
    }

    public static boolean leerBoolean(String texto) {
        int i = JOptionPane.showConfirmDialog(null, texto, "Consulta", JOptionPane.YES_NO_OPTION);
        return i == JOptionPane.YES_OPTION;
    }

    public static void mostrarString(String s) {
        JOptionPane.showMessageDialog(null, s);
    }

    public static String leerPassword(String texto) {
        final JPasswordField pwd = new JPasswordField();
        ActionListener al = new ActionListener() {

            public void actionPerformed(ActionEvent ae) {
                pwd.requestFocusInWindow();
            }
        };
        Timer timer = new Timer(200, al);
        timer.setRepeats(false);
        timer.start();
        Object[] objs = {texto, pwd};
        String password = "";
        if (JOptionPane.showConfirmDialog(null, objs, "Entrada",
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE) == JOptionPane.OK_OPTION) {
            password = String.valueOf(pwd.getPassword());
        }
        return password;
    }

    public static LocalDate leerFecha(String texto) {
        boolean correcto = false;
        LocalDate fecha = null;
        
        while(!correcto) {
            try {
                String st = JOptionPane.showInputDialog(texto);
                fecha = LocalDate.parse(st);
                correcto = true;
            }
            catch(DateTimeParseException ex){}
        }
        
        return fecha;
    }
    
    public static LocalTime leerHora(String texto) {
        boolean correcto = false;
        LocalTime hora = null;
        
        while(!correcto) {
            try {
                String st = JOptionPane.showInputDialog(texto);
                hora = LocalTime.parse(st);
                correcto = true;
            }
            catch(DateTimeParseException ex){}
        }
        return hora;
    }
}
