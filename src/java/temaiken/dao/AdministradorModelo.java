/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.dao;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.*;
import temaiken.bean.UserBean;
/**
 *
 * @author andra
 */
public class AdministradorModelo {

    private String jdbcDriver;
    private String dbName;
    private String urlRoot;
    private UserBean resultado;
    private ActionListener listener;

    public AdministradorModelo(String url, String dbName) {
        jdbcDriver = "com.mysql.jdbc.Driver";
        urlRoot = "jdbc:mysql://" + url + "/";
        this.dbName = dbName;
        listener = null;
        resultado = new UserBean();
        try {
            Class.forName(jdbcDriver);
        } catch (ClassNotFoundException e) {
            reportException(e.getMessage());
        }
    }

    public void consultarDatos(String u) {
        try {
            Connection con = DriverManager.getConnection(urlRoot + dbName, "", "");
            Statement stmt = con.createStatement();
            stmt.execute(
                    "SELECT usuario, nombre, direccion, telefono, fechaIngreso, rol "
                            + "FROM usuarios "
                            + "WHERE usuario='" + u );
            ResultSet rs = stmt.getResultSet();
            while (rs.next()) {
                UserBean usuario = new UserBean();
                usuario.setUser(rs.getString(1));
                usuario.setPass(rs.getString(2));
                usuario.setRol(rs.getString(3));
                resultado = usuario;
            }
            con.close();
        } catch (SQLException e) {
            reportException(e.getMessage());
        }

    }

    public UserBean getResultado() {
        return resultado;
    }

    private void reportException(String exception) {
        if (listener != null) {
            ActionEvent evt = new ActionEvent(this, 0, exception);
            listener.actionPerformed(evt);
        }
    }

    public void addExceptionListener(ActionListener listener) {
        this.listener = listener;
    }
}

