package temaiken.dao;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import temaiken.bean.EspeciesBean;
import temaiken.bean.UserBean;
import temaiken.servlets.Controlador;

/**
 *
 * @author bianca
 */
public class EspeciesDAO {
    
    private EspeciesBean bean;
    private List<EspeciesBean> beanList;
    
     public EspeciesDAO() {
        bean = new EspeciesBean();
        beanList = new ArrayList<>();
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(CuidadorDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     
     public List<EspeciesBean> getEspeciesByUser(String user){
         beanList.clear();
         
         String query = "SELECT e.nombre_es, e.nombre_cient, e.descripcion, ce.fecha_cuidado "
                 + "FROM especies e "
                 + "JOIN cuidador_especie ce ON e.id_especie = ce.id_especie "
                 + "JOIN usuarios u ON u.id = ce.id_cuidador "
                 + "WHERE u.usuario = '" + user + "';";
         
        try {
            Statement stmt = MyDBConnection.getConnection().createStatement();
            ResultSet rs = stmt.executeQuery(query);
            
            while(rs.next()){
                bean = new EspeciesBean();
                bean.setEspecie(rs.getString(1).toUpperCase());
                bean.setNombreCientifico(rs.getString(2).toUpperCase());
                bean.setDescripcion(rs.getString(3).toUpperCase());
                bean.setEsCuidadoDesde(rs.getString(4));
                bean.setCuidador(Controlador.getUser().toUpperCase());
                
                beanList.add(bean);
            }
            
            rs.close();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(EspeciesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
         return beanList;
     }

}
