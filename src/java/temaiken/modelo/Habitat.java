/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Iara
 */
public class Habitat implements Serializable {
    private String nombre;
    private String clima;
    private String vegetacion;
    private String continente;
    
    public Habitat(String nombre, String clima, String vegetacion, String continente){
        this.nombre = nombre;
        this.clima = clima;
        this.vegetacion = vegetacion;
        this.continente = continente;
    } 

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the clima
     */
    public String getClima() {
        return clima;
    }

    /**
     * @param clima the clima to set
     */
    public void setClima(String clima) {
        this.clima = clima;
    }

    /**
     * @return the vegetacion
     */
    public String getVegetacion() {
        return vegetacion;
    }

    /**
     * @param vegetacion the vegetacion to set
     */
    public void setVegetacion(String vegetacion) {
        this.vegetacion = vegetacion;
    }

    /**
     * @param continente the continente to set
     */
    public void setContinente(String continente) {
        this.continente = continente;
    }

    /**
     * @return the continente
     */
    public String getContinente() {
        return continente;
    }
}
