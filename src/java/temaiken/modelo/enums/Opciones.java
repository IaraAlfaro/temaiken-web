/*
 * 
 * 
 * 
 */
package temaiken.modelo.enums;

/**
 *
 * @author bianca
 */
public enum Opciones {
    
    CONSULTAR_DATOS ("datos"),
    CONSULTAR_ITINERARIOS ("consultarItinerarios"),
    CONSULTAR_ESPECIES ("consultarEspecie"),
    SALIR ("salir");
    
    private String option;
    
    Opciones (String op){
        this.option = op;
    }

    public String getOption(){
        return this.option;
    }
    
}
