
package temaiken.modelo.enums;

/**
 *
 * @author bianca
 */
public enum Roles {
    
    GUIA ("guia"),
    CUIDADOR ("cuidador"),
    ADMIN ("administrador");
    
    private String role;
    
    Roles(String rol){
        this.role = rol;
    }
    
    public String getRol(){
        return this.role;
    }
}
