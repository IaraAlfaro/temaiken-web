/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Iara
 */
public class Itinerario implements Serializable{
    private int codigo;
    private int duracionM;
    private int longitud;
    private int maxVisitantes;
    private ArrayList<Zona> zonas;
    
    public Itinerario(int codigo, int duracionM, int longitud, int maxVisitantes) {
        this.codigo = codigo;
        this.duracionM = duracionM;
        this.longitud = longitud;
        this.maxVisitantes = maxVisitantes;
        this.zonas = new ArrayList<>();
    }

    /**
     * @return the codigo
     */
    public int getCodigo() {
        return codigo;
    }

    /**
     * @param codigo the codigo to set
     */
    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    /**
     * @return the duracionM
     */
    public int getDuracionM() {
        return duracionM;
    }

    /**
     * @param duracionM the duracionM to set
     */
    public void setDuracionM(int duracionM) {
        this.duracionM = duracionM;
    }

    /**
     * @return the longitud
     */
    public int getLongitud() {
        return longitud;
    }

    /**
     * @param longitud the longitud to set
     */
    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    /**
     * @return the maxVisitantes
     */
    public int getMaxVisitantes() {
        return maxVisitantes;
    }

    /**
     * @param maxVisitantes the maxVisitantes to set
     */
    public void setMaxVisitantes(int maxVisitantes) {
        this.maxVisitantes = maxVisitantes;
    }

    /**
     * @return the zonas
     */
    public ArrayList<Zona> getZonas() {
        return zonas;
    }

    /**
     * @param zonas the zonas to set
     */
    public void setZonas(ArrayList<Zona> zonas) {
        this.zonas = zonas;
    }

    public int calcularEspecies() {
        int contador = 0;
        
        for (int i = 0; i < zonas.size(); i++) {
            Zona zona = zonas.get(i);
            contador += zona.getEspecies().size();
        }
        
        return contador;
    }
    
}
