/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 *
 * @author Iara
 */
public class Zona implements Serializable {
    private String nombre;
    private int extension;
    private ArrayList<Especie> especies;
    
    public Zona(String nombre, int extension){
        this.nombre = nombre;
        this.extension = extension;
        this.especies = new ArrayList<>();
    }

    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * @return the extension
     */
    public int getExtension() {
        return extension;
    }

    /**
     * @param extension the extension to set
     */
    public void setExtension(int extension) {
        this.extension = extension;
    }

    /**
     * @return the especies
     */
    public ArrayList<Especie> getEspecies() {
        return especies;
    }

    /**
     * @param especies the especies to set
     */
    public void setEspecies(ArrayList<Especie> especies) {
        this.especies = especies;
    }

    String mostrarEspecies() {
        String lista = "";
        for(int i = 0; i < especies.size(); i++) {
            Especie e = especies.get(i);
            lista += " " + i + " - " + e.getNombreCientifico() + "\n";
        }
        
        return lista;
    }
}
