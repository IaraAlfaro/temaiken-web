/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import temaiken.vista.EntradaSalida;

/**
 *
 * @author Iara
 */
public class Cuidador extends Usuario implements Serializable{
    private ArrayList<CuidadorEspecie> cuidadorEspecies;
            
    public Cuidador(String nombre, String direccion, String telefono,
            LocalDate fechaIngreso, String usuario, String password) {
        super(nombre, direccion, telefono, fechaIngreso, usuario, password);
     
        cuidadorEspecies = new ArrayList<>();
    }
    
    /**
     *
     * @param temaiken
     */
    @Override
    public void proceder(Temaiken temaiken) {
        EntradaSalida.mostrarString("Bienvenido cuidador " + this.nombre);
        char opcion;
        
        do{
            do {
             opcion = EntradaSalida.leerChar(
                    "1 - Ver mis datos personales\n" +
                    "2 - Ver mis especies asignadas\n" +
                    "3 - Salir" +
                    "Ingrese la opción deseada"
            );
            }
            while(opcion > '3' || opcion < '1');

            switch(opcion){
                case '1':
                    EntradaSalida.leerString("Nombre: " + nombre + "\n" +
                    "Dirección: " + direccion + "\n" +
                    "Teléfono: " + telefono + "\n" + 
                    "Fecha de ingreso: " + fechaIngreso + "\n");
                    break;

                case '2':
                    String listaEspecies = listarEspecies();
                    listaEspecies += "\nIngrese el numero de la especie";

                    CuidadorEspecie ce = null;

                    while(ce == null){
                        int numeroEspecie = EntradaSalida.leerInt(listaEspecies);

                        if(numeroEspecie >= 0 && numeroEspecie < cuidadorEspecies.size()){
                            ce = cuidadorEspecies.get(numeroEspecie);
                        }
                    }

                    EntradaSalida.leerString(ce.detallar());
                    break;
            }
        }
        while(opcion != 3);
    }
    
    
    
    /**
     * @return the cuidadorEspecies
     */
    public ArrayList<CuidadorEspecie> getCuidadorEspecies() {
        return cuidadorEspecies;
    }

    /**
     * @param cuidadorEspecies the cuidadorEspecies to set
     */
    public void setCuidadorEspecies(ArrayList<CuidadorEspecie> cuidadorEspecies) {
        this.cuidadorEspecies = cuidadorEspecies;
    }

    public String listarEspecies() {
        
        String lista = "";
        for(int i = 0; i < cuidadorEspecies.size(); i++) {
            CuidadorEspecie ce = this.cuidadorEspecies.get(i);
            
            lista += " " + i + " - " + ce.getEspecie().getNombreCientifico() + "\n";
        }
        
        return lista;
    }
    
}
