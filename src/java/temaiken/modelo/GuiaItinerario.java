/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.time.LocalTime;

/**
 *
 * @author Iara
 */
class GuiaItinerario implements Serializable{
    private LocalTime horario;
    private Itinerario itinerario;
    
    public GuiaItinerario(LocalTime horario, Itinerario itinerario){
        this.horario = horario;
        this.itinerario = itinerario;
    }

    /**
     * @return the horario
     */
    public LocalTime getHorario() {
        return horario;
    }

    /**
     * @param horario the horario to set
     */
    public void setHorario(LocalTime horario) {
        this.horario = horario;
    }

    /**
     * @return the itinerario
     */
    public Itinerario getItinerario() {
        return itinerario;
    }

    /**
     * @param itinerario the itinerario to set
     */
    public void setItinerario(Itinerario itinerario) {
        this.itinerario = itinerario;
    }
    
    public String detallar() {
       return "Codigo: " + itinerario.getCodigo() + "\n" +
               "Duración: " + itinerario.getDuracionM() + "\n" +
               "Longitud: " + itinerario.getLongitud() + " metros" + "\n" + 
               "Visitantes: " + itinerario.getMaxVisitantes() + "\n" + 
               "Especies: " + itinerario.calcularEspecies() + "\n";
    }
    
    
}
