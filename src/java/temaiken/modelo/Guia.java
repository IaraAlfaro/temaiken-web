/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package temaiken.modelo;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import temaiken.vista.EntradaSalida;

/**
 *
 * @author Iara
 */
public class Guia extends Usuario implements Serializable{
    private ArrayList<GuiaItinerario> guiaItinerarios;
    
    public Guia(String nombre, String direccion, String telefono,
            LocalDate fechaIngreso, String usuario, String password) {
        super(nombre, direccion, telefono, fechaIngreso, usuario, password);
        
        this.guiaItinerarios = new ArrayList<>();
    }    
    
    /**
     *
     * @param temaiken
     */
    @Override
    public void proceder(Temaiken temaiken) {
        char opcion;
        
        EntradaSalida.mostrarString("Bienvenid@ guía " + this.nombre);
        
        do{
            do {
                opcion = EntradaSalida.leerChar(
                        "1 - Ver mis datos personales\n" +
                        "2 - Ver mis itinerarios\n" +
                        "3 - Salir" +
                        "Ingrese la opción deseada"
                );
            }
            while(opcion > '3' || opcion < '1');

            switch(opcion){
                case '1':
                    EntradaSalida.mostrarString("Nombre: " + nombre + "\n" +
                    "Dirección: " + direccion + "\n" +
                    "Teléfono: " + telefono + "\n" + 
                    "Fecha de ingreso: " + fechaIngreso + "\n");
                    break;

                case '2':
                    String listaItinerarios = listarItinerarios();
                    listaItinerarios += "\nIngrese el numero de itinerario";

                    GuiaItinerario gi = null;

                    while(gi == null){
                        int numeroItinerario = EntradaSalida.leerInt(listaItinerarios);

                        if(numeroItinerario >= 0 && numeroItinerario < guiaItinerarios.size()){
                            gi = guiaItinerarios.get(numeroItinerario);
                        }
                    }

                    EntradaSalida.mostrarString(gi.detallar());
                    break;
            }
        }
        while(opcion != 3);
    }

    /**
     * @return the guiaItinerario
     */
    public ArrayList<GuiaItinerario> getGuiaItinerarios() {
        return guiaItinerarios;
    }

    /**
     * @param guiaItinerarios the guiaItinerarios to set
     */
    public void setGuiaItinerarios(ArrayList<GuiaItinerario> guiaItinerarios) {
        this.guiaItinerarios = guiaItinerarios;
    }

    public String listarItinerarios() {
        String lista = "";
        for(int i = 0; i < this.guiaItinerarios.size(); i++) {
            GuiaItinerario gi = this.guiaItinerarios.get(i);
            
            lista += " " + i + " - " + gi.getItinerario().getCodigo() + " " + gi.getHorario() + "\n";
        }
        
        return lista;
    }
    
}
